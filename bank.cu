#include "murmurhash2.cuh"

#include <cuda_runtime.h>
#include <curand.h>
#include <curand_kernel.h>
#include "helper_timer.h"

#define PR_rand(n) \
	PR_i_rand(args, n) \
//

#ifdef PR_ARGS_S_EXT
#undef PR_ARGS_S_EXT
#endif

#define PR_ARGS_S_EXT \
typedef struct { \
		curandState *state; \
	} pr_args_ext_t \
//
#ifdef PR_DEV_BUFF_S_EXT
#undef PR_DEV_BUFF_S_EXT
#endif

#define PR_DEV_BUFF_S_EXT \
	typedef struct { \
		curandState *state; \
	} pr_dev_buff_ext_t \
//

#ifdef PR_BEFORE_RUN_EXT
#undef PR_BEFORE_RUN_EXT
#endif

#define PR_BEFORE_RUN_EXT(args) ({ \
	pr_dev_buff_ext_t *cuRandBuf; \
	args->host.pr_args_ext = malloc(sizeof(pr_dev_buff_ext_t)); \
	cuRandBuf = (pr_dev_buff_ext_t*)args->host.pr_args_ext; \
	PR_ALLOC(args->dev.pr_args_ext, sizeof(pr_dev_buff_ext_t)); \
	PR_ALLOC(cuRandBuf->state, PR_blockNum * PR_threadNum * sizeof(curandState)); \
	cudaFuncSetCacheConfig(setupKernel, cudaFuncCachePreferL1); \
	setupKernel <<< PR_blockNum, PR_threadNum >>>(cuRandBuf->state); \
	cudaThreadSynchronize(); \
	PR_CPY_TO_DEV(args->dev.pr_args_ext, args->host.pr_args_ext, sizeof(pr_dev_buff_ext_t)); \
}) \
//

#ifdef PR_AFTER_RUN_EXT
#undef PR_AFTER_RUN_EXT
#endif

#define PR_AFTER_RUN_EXT(args) ({ \
	cudaFree(((pr_dev_buff_ext_t*)args->host.pr_args_ext)->state); \
	cudaFree(args->dev.pr_args_ext); \
	free(args->host.pr_args_ext); \
}) \
//

#include "pr-stm.cuh"

__global__ void setupKernel(curandState *state);
__device__ unsigned PR_i_rand(pr_tx_args_dev_host_s args, unsigned n);

#include "pr-stm-internal.cuh" // implementation

// --------------------
__constant__ PR_DEVICE unsigned PR_seed = 1234; // TODO: set seed

__global__ void setupKernel(curandState *state) {
	int id = threadIdx.x + blockDim.x*blockIdx.x;
	curand_init(PR_seed, id, 0, &state[id]);
}

__device__ unsigned PR_i_rand(pr_tx_args_dev_host_s args, unsigned n)
{
	curandState *state = ((pr_dev_buff_ext_t*)args.pr_args_ext)->state;
	int id = PR_THREAD_IDX;
	int x = 0;
	/* Copy state to local memory for efficiency */
	curandState localState = state[id];
	x = curand(&localState);
	state[id] = localState;
	return x % n;
}
// --------------------

#include <iostream>
#include <fstream>
#include <cstdio>
#include <ctime>

//For testing purposses
#define firstXEn           0    //Set to 1 to force transactions to happen between the firstX accounts
#define firstX             200  //Used by firstXEn

#define hashNum            1    // how many accounts shared 1 lock
#define BANK_NB_ACCOUNTS   2621440 //total accounts number 10M = 2621440 integer
//#define PR_threadNum 128 //threads number
#define PR_WRITE_ONLY_THRS 0   // how many threads do all write
#define PR_READ_ONLY_THRS  0   // how many threads do all read
//#define PR_blockNum 5	//block number
#define BANK_NB_TRANSFERS  2   // transfer money between 2 accounts
#define TransEachThread    50  // how many transactions each thread do
#define iterations         50  // loop how many times

using namespace std;

// random Function random several different numbers and store them into idx(Local array which stores idx of every source in Global memory).
__device__ void random_Kernel(PR_txCallDefArgs, int * idx, curandState* state, int size)
{
	int i, j;

	for (i = 0; i < BANK_NB_TRANSFERS; i++)
	{
		int m = 0;
		while (m < 1){
			idx[i] = PR_rand(size);
#if firstXEn==1
			idx[i] = PR_rand(firstX);
#endif
			bool hasEqual = 0;
			for (j = 0; j < i; j++)	//random different numbers
			{
				if (idx[i] == idx[j])
				{
					hasEqual = 1;
					break;
				}
			}
			if (hasEqual != 1)	//make sure they are different
				m++;
		}
	}
	/*idx[0] = generate_kernel(state,100)%size;
	for (int i = 0; i < BANK_NB_TRANSFERS; i++)
	{
	idx[i] = (idx[0]+i)%size;
	}*/
}

/*
* Isto e' o que interessa
*/
__global__ void addKernelTransaction2(PR_globalKernelArgs)
{
	PR_enterKernel();

	int i = 0, j;	//how many transactions one thread need to commit
	int target;
	PR_GRANULE_T nval;
	int idx[BANK_NB_TRANSFERS];
	PR_GRANULE_T reads[BANK_NB_TRANSFERS];
	PR_GRANULE_T *accounts = (PR_GRANULE_T*)args.inBuf;
	size_t nbAccounts = args.inBuf_size / sizeof(PR_GRANULE_T);
	curandState *state = ((pr_dev_buff_ext_t*)args.pr_args_ext)->state;

	random_Kernel(PR_txCallArgs, idx, state, nbAccounts);	//get random index

	while (i++ < TransEachThread) { // each thread need to commit x transactions
		PR_txBegin();

		for (j = 0; j < BANK_NB_TRANSFERS; j++)	{ //open read all accounts from global memory
//			reads[j] = accounts[idx[j]];
			reads[j] = PR_read(accounts + idx[j]);
			// if (pr_args.is_abort) break;
		}
		if (pr_args.is_abort) { PR_txRestart(); } // optimization

		for (j = 0; j < BANK_NB_TRANSFERS / 2; j++) {
			target = j*2;
			nval = reads[target] - 1; // -money
//			accounts[idx[target]] = nval;
			PR_write(accounts + idx[target], nval); //write changes to write set
			if (pr_args.is_abort) break;

			target = j*2+1;
			nval = reads[target] + 1; // +money
//			accounts[idx[target]] = nval;
			PR_write(accounts + idx[target], nval); //write changes to write set
			if (pr_args.is_abort) break;
		}
		if (pr_args.is_abort) { PR_txRestart(); } // optimization
		PR_txCommit();
	}

	PR_exitKernel();
}

int main(int argc, char* argv[])
{
	double avgKernelTime = 0.0;
	double avgAborts     = 0.0;
	double avgCommits    = 0.0;

	if (argc > 2) {
		PR_blockNum = atoi(argv[1]);
		PR_threadNum = atoi(argv[2]) % 1025;
	}

	for (int j = 0; j < 1; j++) {
		printf("Blocks: %d, Threads: %d\n", PR_blockNum, PR_threadNum);

		//int size = (BANK_NB_ACCOUNTS + 2000000 * j);
		int size = BANK_NB_ACCOUNTS;
		int sum;
		int *a = new int[size];	//accounts array
		pr_buffer_s inBuf, outBuf;

		printf("My Algorithm for %d accounts(transfer among %d accounts) with %d threads!!\n", size, BANK_NB_ACCOUNTS, PR_threadNum*PR_blockNum);
		/*int *state = new int[size];	//locks array
		printf("222!!\n");*/

		for (int i = 0; i < size; i++) { a[i] = 10; }
		sum = 0;
		for (int i = 0; i < size; i++) { sum += a[i]; }
		printf("sum before = %d\n", sum);

		// Add vectors in parallel.
		// TODO: new interface
		pr_tx_args_s prArgs;
		PR_init();
		PR_CHECK_CUDA_ERROR(cudaMalloc((void**)&inBuf.buf, BANK_NB_ACCOUNTS * sizeof(int)), "cudaMalloc");
		PR_createStatistics(&prArgs);

		for (int i = 0; i < iterations; ++i) {
			inBuf.size = BANK_NB_ACCOUNTS * sizeof(int);
			PR_CPY_TO_DEV(inBuf.buf, a, inBuf.size);
			outBuf.buf = NULL;
			outBuf.size = 0;
			PR_prepareIO(&prArgs, inBuf, outBuf);
			PR_run(addKernelTransaction2, &prArgs, NULL); // aborts on fail
			PR_CPY_TO_HOST(a, inBuf.buf, inBuf.size); // this serializes
		}
		PR_retrieveIO(&prArgs, NULL); // updates PR_nbAborts and PR_nbCommits
		avgCommits = PR_nbCommits / (double)iterations;
		avgAborts = PR_nbAborts / (double)iterations;
		avgKernelTime = PR_kernelTime / (double)iterations;

		PR_disposeIO(&prArgs);
		PR_teardown();

		PR_CHECK_CUDA_ERROR(cudaFree(inBuf.buf), "free");

		sum = 0;
		for (int i = 0; i < size; i++) { sum += a[i]; }
		printf("sum after = %d\n", sum);

		double abort_rate = avgAborts*iterations / (TransEachThread*PR_threadNum); // TODO:
		double throughput = PR_blockNum*PR_threadNum*TransEachThread / ((avgKernelTime) / 1000);
		printf("Time for the kernel: %f ms\n", avgKernelTime);
		printf("Nb. Aborts (avg)  = %f \n", avgAborts);
		printf("Nb. Commits (avg) = %f \n", avgCommits);
		printf("Abort rate  = %f \n", abort_rate);
		printf("throughput is = %lf txs/second\n", throughput);

		FILE *stats_file_fp = fopen("stats.txt", "a");
		if (ftell(stats_file_fp) < 1) {
			fprintf(stats_file_fp, "#"
				"BLOCKS\t"
				"THREADS\t"
				"THROUGHPUT\t"
				"PROB_ABORT\n"
			);
		}
		fprintf(stats_file_fp, "%i\t", PR_blockNum);
		fprintf(stats_file_fp, "%i\t", PR_threadNum);
		fprintf(stats_file_fp, "%.0lf\t", throughput);
		fprintf(stats_file_fp, "%lf\n", avgAborts/(avgAborts+avgCommits));
		fclose(stats_file_fp);

		//printf("max: %d\n",(numeric_limits<int>::max)());
		PR_CHECK_CUDA_ERROR(cudaDeviceReset(), "cudaDeviceReset failed!");
		delete[] a;
	}
	return 0;
}
