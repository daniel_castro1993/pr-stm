### CONFIG
CUDA_UTIL_DIR ?= ~/projs/cuda-utils
CUDA_DIR      ?= /usr/local/cuda
TESTS_DIR     ?= ./tests

INCLUDES := -I ./include -I $(CUDA_UTIL_DIR)/include
DEFINES  :=
LIBS     :=

CFLAGS   := -g -c -std=gnu99
CXXFLAGS := -g -c -std=c++11
NVFLAGS  := -c -std=c++11 --default-stream per-thread -arch sm_30 
LDFLAGS  :=
NVCC     := $(CUDA_DIR)/bin/nvcc

SRCS     := $(shell ls -rt -d -1 ./src/*.c ./src/*.cpp ./src/*.cu 2>/dev/null)

APP_SRC  := ./bank.cu
APP      := ./bank
SRCS     += $(APP_SRC)
OBJS     := $(addsuffix .o, $(basename $(SRCS)))

CFLAGS   += $(INCLUDES) $(DEFINES)
CXXFLAGS += $(INCLUDES) $(DEFINES)
NVFLAGS  += $(INCLUDES) $(DEFINES)
LDFLAGS  += $(LIBS)

### RULES

all: $(APP)
	$(MAKE) $(TESTS_DIR)

%.o: %.cu
	$(NVCC) $(NVFLAGS) -o $@ $^

$(APP): $(OBJS)
	$(NVCC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(APP) $(OBJS)
