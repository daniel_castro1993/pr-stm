#ifndef PR_STM_I_H_GUARD
#define PR_STM_I_H_GUARD

#include "pr-stm.cuh"

#include <curand_kernel.h>
#include <cuda_profiler_api.h>
#include "helper_cuda.h"
#include "helper_timer.h"

PR_ARGS_S_EXT;
PR_DEV_BUFF_S_EXT;

// Global vars
PR_globalVars;

// ----------------------------------------
// Utility defines
#define PR_ADD_TO_ARRAY(array, size, value) ({ \
	(array)[(size)++] = (value); \
})

#define PR_ADD_TO_LOG(log, value) ({ \
	((void**)log.buf)[log.size++] = ((void*)value); \
})

// assume find ran first
#define PR_RWSET_GET_VAL(rwset, idx, buf, size) \
	(*buf = (rwset).values[idx]) \
//

#define PR_RWSET_SET_VAL(rwset, idx, buf, size) \
	((rwset).values[idx] = *buf) \
//

#define PR_RWSET_SET_VERSION(rwset, idx, version) \
	((rwset).versions[idx] = (int)version) \
//

#define PR_ADD_TO_RWSET(rwset, addr, version, value, size_val) \
	(rwset).addrs[(rwset).size] = (PR_GRANULE_T*)addr; \
	PR_RWSET_SET_VAL(rwset, (rwset).size, value, size_val); \
	PR_RWSET_SET_VERSION(rwset, (rwset).size, version); \
	(rwset).size += 1; \
//

#define PR_FIND_IN_RWSET(rwset, addr) ({ \
	int i; \
	PR_GRANULE_T* a = (PR_GRANULE_T*)addr; \
	long res = -1; \
	for (i = 0; i < (rwset).size; i++) { \
		if ((rwset).addrs[i] == a) { \
			res = i; \
			break; \
		} \
	} \
	res; \
})

// ----------------------------------------

//openRead Function reads data from global memory to local memory. args->rset_vals stores value and args->rset_versions stores version.
PR_DEVICE void PR_i_openReadKernel(
	PR_args_s *args, PR_GRANULE_T *addr, PR_GRANULE_T *rbuf, size_t size
) {
	int j, k, temp, version;
	// volatile int *data = (volatile int*)args->data;
	// int target = args->rset_addrs[args->rset_size];

	temp = PR_GET_MTX(args->mtx, addr);
	k = PR_FIND_IN_RWSET(args->wset, addr);

	if (!args->is_abort && !PR_CHECK_PRELOCK(temp)) {
		// not locked
		if (k != -1) {
			// in wset
			PR_RWSET_GET_VAL(args->wset, k, rbuf, size);
		} else {
			j = PR_FIND_IN_RWSET(args->rset, addr);
			version = PR_GET_VERSION(temp);
			if (j != -1) {
				// in rset
				// TODO: this seems a bit different in the paper
				PR_RWSET_GET_VAL(args->rset, j, rbuf, size);
				PR_RWSET_SET_VERSION(args->rset, j, version);
			} else {
				// not found
				PR_SET(rbuf, *addr, sizeof(PR_GRANULE_T));
				PR_ADD_TO_RWSET(args->rset, addr, version, rbuf, size);
			}
		}
	} else {
		PR_SET(rbuf, *addr, sizeof(PR_GRANULE_T));
		args->is_abort = 1;
	}
}

PR_DEVICE void PR_i_openWriteKernel(
	PR_args_s *args, PR_GRANULE_T *addr, PR_GRANULE_T *wbuf, size_t size
) {
	int j, k;
	int temp, version;

	temp = PR_GET_MTX(args->mtx, addr);
	if (!args->is_abort && !PR_CHECK_PRELOCK(temp)) {
		// not locked --> safe to access TODO: the rset seems redundant
		j = PR_FIND_IN_RWSET(args->rset, addr); // check in the read-set
		k = PR_FIND_IN_RWSET(args->wset, addr); // check in the write-set
		version = PR_GET_VERSION(temp);

		if (j == -1) {
			// not in the read-set
			PR_ADD_TO_RWSET(args->rset, addr, version, wbuf, size);
		} else {
			// update the read-set (not the version) TODO: is this needed?
			PR_RWSET_SET_VAL(args->rset, j, wbuf, size);
		}

		if (k == -1) {
			// does not exist in write-set
			PR_ADD_TO_RWSET(args->wset, addr, version, wbuf, size);
		} else {
			// update the write-set
			PR_RWSET_SET_VAL(args->wset, j, wbuf, size);
		}
	} else {
		// already locked
		args->is_abort = 1;
	}
}

PR_DEVICE int PR_i_tryPrelock(PR_args_s *args)
{
	int i, res = 0;
	PR_GRANULE_T *addr;
	int version, lock;
	bool validVersion, notLocked, ownerIsSelf;

	for (i = 0; i < args->rset.size; i++) {
		addr = args->rset.addrs[i];
		version = args->rset.versions[i];
		lock = PR_GET_MTX(args->mtx, addr);
		validVersion = PR_GET_VERSION(lock) == version;
		notLocked = !PR_CHECK_PRELOCK(lock);
		ownerIsSelf = PR_GET_OWNER(lock) == PR_THREAD_IDX;
		if (validVersion && (notLocked || ownerIsSelf)) {
			res++;
			continue;
		} else {
			res = -1; // did not validate
			break;
		}
	}

	return res;
}

PR_DEVICE void PR_i_unlockWset(PR_args_s *args)
{
	int i;
	int *lock, lval, nval;

	for (i = 0; i < args->wset.size; i++) {
		lock = (int*) &(PR_GET_MTX(args->mtx, args->wset.addrs[i]));
		lval = *lock;
		nval = lval & PR_MASK_VERSION;
		if (PR_GET_OWNER(lval) == PR_THREAD_IDX)
		atomicCAS(lock, lval, nval);
	}
}

PR_DEVICE void
PR_i_validateKernel(PR_args_s *args)
{
	int vr = 0; // flag for how many values in read  set is still validate
	int vw = 0; // flag for how many values in write set is still validate
	int i, k;
	int *lock, *old_lock, new_lock, lval, oval, nval;

	int isLocked, isPreLocked, ownerIsSelf, ownerNotSelf, validVersion;

	if (args->is_abort) return;

	vr = PR_i_tryPrelock(args);
	if (vr == -1) {
		args->is_abort = 1;
		return; // abort
	}

	__threadfence();

	for (i = 0; i < args->wset.size; i++) {
		while (1) {
			// spin until this thread can lock one account in write set

			lock = (int*) &(PR_GET_MTX(args->mtx, args->wset.addrs[i]));
			lval = *lock;

			//check if version changed or locked by higher priority thread
			isLocked = PR_CHECK_LOCK(lval);
			isPreLocked = PR_CHECK_PRELOCK(lval);
			ownerNotSelf = PR_GET_OWNER(lval) < PR_THREAD_IDX;
			validVersion = PR_GET_VERSION(lval) != args->rset.versions[i];

			if (isLocked || (isPreLocked && ownerNotSelf) || validVersion) {
				// if one of accounts is locked by a higher priority thread
				// or version changed, unlock all accounts it already locked
				for (k = 0; k < i; k++) {
					old_lock = (int*) &(PR_GET_MTX(args->mtx, args->wset.addrs[k]));
					oval = *old_lock;

					// check if this account is still locked by itself
					isPreLocked = PR_CHECK_PRELOCK(oval);
					ownerIsSelf = PR_GET_OWNER(oval) == PR_THREAD_IDX;
					if (isPreLocked && ownerIsSelf) {
						nval = oval & PR_MASK_VERSION;
						atomicCAS(old_lock, oval, nval);
					}
				}
				// if one of accounts is locked by a higher priority thread or version changed, return false
				args->is_abort = 1;
				return;
			}
			new_lock = PR_PRELOCK_VAL(args->rset.versions[i], PR_THREAD_IDX);

			// atomic lock that account
			if (atomicCAS(lock, lval, new_lock) == lval) break;
		}
	}

	__threadfence();
	// if this thread can pre-lock all accounts it needs to, really lock them
	for (i = 0; i < args->wset.size; i++) {
	 	lock = (int*) &(PR_GET_MTX(args->mtx, args->wset.addrs[i])); //get lock,owner,and version from global memory
		lval = *lock;

		// if it is still locked by itself (check lock flag and owner position)
		ownerIsSelf = PR_GET_OWNER(lval) == PR_THREAD_IDX; // TODO: should be "self"
		if (ownerIsSelf) {
			new_lock = PR_LOCK_VAL(args->wset.versions[i], PR_THREAD_IDX); // temp final lock
			if (atomicCAS(lock, lval, new_lock) == lval) {
				vw++;	// if succeed, vw++
			} else {
				// failed to lock
				PR_i_unlockWset(args);
				args->is_abort = 1;
				return;
			}
		} else {
			// cannot final lock --> unlock all
			PR_i_unlockWset(args);
			args->is_abort = 1;
			return;
		}
	}
	if (vw == args->wset.size && vr == args->rset.size) {
		// all locks taken (should not need the extra if)
		PR_AFTER_VAL_LOCKS_EXT(args);
	}
}

PR_DEVICE void PR_i_commitKernel(PR_args_s *args)
{
	int i;
	int *lock, lval, nval;
	PR_GRANULE_T *addr, val;

	// write all values in write set back to global memory and increase version
	for (i = 0; i < args->wset.size; i++) {
		addr = args->wset.addrs[i];
		val = args->wset.values[i]; // TODO: variable size
		PR_SET(addr, val, PR_LOCK_GRANULARITY);
		PR_AFTER_WRITEBACK_EXT(args, i, addr, val);
	}
	__threadfence();
	for (i = 0; i < args->wset.size; i++) {
		lval = PR_LOCK_VAL(args->wset.versions[i], PR_THREAD_IDX);
		lock = (int*) &(PR_GET_MTX(args->mtx, args->wset.addrs[i]));
		if (args->wset.versions[i] < 2048) { //increase version(if version is going to overflow, change it to 0)
			nval = (args->wset.versions[i] + 1) << 21;
		} else {
			nval = 0;
		}
		atomicCAS(lock, lval, nval);
	}
}

PR_HOST void PR_noStatistics(pr_tx_args_s *args)
{
	args->host.nbAborts = NULL;
	args->host.nbCommits = NULL;
	args->dev.nbAborts = NULL;
	args->dev.nbCommits = NULL;
}

PR_HOST void PR_createStatistics(pr_tx_args_s *args)
{
	int nbThreads = PR_blockNum * PR_threadNum;
	int sizeArray = nbThreads * sizeof(int);

	args->host.nbAborts = NULL; // not used
	args->host.nbCommits = NULL;

	PR_ALLOC(args->dev.nbAborts, sizeArray);
	PR_ALLOC(args->dev.nbCommits, sizeArray);

	PR_resetStatistics(args, NULL);
}

PR_HOST void PR_resetStatistics(pr_tx_args_s *args, void *strm)
{
	int nbThreads = PR_blockNum * PR_threadNum;
	int sizeArray = nbThreads * sizeof(int);
	cudaStream_t stream = (cudaStream_t)strm;

	PR_CHECK_CUDA_ERROR(cudaMemsetAsync(args->dev.nbAborts, 0, sizeArray, stream), "");
	PR_CHECK_CUDA_ERROR(cudaMemsetAsync(args->dev.nbCommits, 0, sizeArray, stream), "");
	PR_nbCommits = 0;
	PR_nbAborts = 0;
}

PR_HOST void PR_prepareIO(
	pr_tx_args_s *args,
	pr_buffer_s inBuf,
	pr_buffer_s outBuf
) {
	// input
	args->dev.inBuf = inBuf.buf;
	args->dev.inBuf_size = inBuf.size;
	args->host.inBuf = NULL; // Not needed
	args->host.inBuf_size = 0;

	// output
	args->dev.outBuf = outBuf.buf;
	args->dev.outBuf_size = outBuf.size;
	args->host.outBuf = NULL; // Not needed
	args->host.outBuf_size = 0;
}

PR_HOST void PR_i_cudaPrepare(
	pr_tx_args_s *args,
	void(*callback)(pr_tx_args_dev_host_s)
) {
	args->host.mtx = PR_lockTableHost;
	args->dev.mtx  = PR_lockTableDev;
	args->callback = callback;
}

PR_HOST void PR_retrieveIO(pr_tx_args_s *args, void *strm)
{
	int i, nbThreads = PR_blockNum * PR_threadNum;
	int sizeArray = nbThreads * sizeof(int);
	// int sizeMtx = PR_LOCK_TABLE_SIZE * sizeof(int);
	int hostNbAborts[nbThreads], hostNbCommits[nbThreads];
	cudaStream_t stream = (cudaStream_t)strm;

	// PR_CPY_TO_HOST(PR_lockTableHost, PR_lockTableDev, sizeMtx); // TODO: only for debug
	PR_CPY_TO_HOST_ASYNC(hostNbAborts, args->dev.nbAborts, sizeArray, stream);
	PR_CPY_TO_HOST_ASYNC(hostNbCommits, args->dev.nbCommits, sizeArray, stream);

	for (i = 0; i < nbThreads; ++i) {
		PR_nbAborts += hostNbAborts[i];
		PR_nbCommits += hostNbCommits[i];
	}
}

PR_HOST void PR_disposeIO(pr_tx_args_s *args)
{
	PR_CHECK_CUDA_ERROR(cudaFree(args->dev.nbAborts), "");
	PR_CHECK_CUDA_ERROR(cudaFree(args->dev.nbCommits), "");
}

PR_HOST void
PR_i_afterStream(cudaStream_t stream, cudaError_t status, void *data)
{
	PR_isDone = 1;
	PR_isStart = 0;
	__sync_synchronize();
}

PR_HOST void
PR_i_run(pr_tx_args_s *args)
{
	// StopWatchInterface *kernelTime = NULL;
	cudaStream_t stream = (cudaStream_t)args->stream;

	cudaFuncSetCacheConfig(args->callback, cudaFuncCachePreferL1);

	PR_isDone = 0;
	PR_isStart = 1;
	__sync_synchronize();
	// pass struct by value
	cudaEventRecord(PR_eventKernelStart); // TODO: does not work in multi-thread
	if (stream != NULL) {
		args->callback<<< PR_blockNum, PR_threadNum, PR_sizeSharedMemory, stream >>>(args->dev);
	} else {
		// serializes
		args->callback<<< PR_blockNum, PR_threadNum, PR_sizeSharedMemory >>>(args->dev);
	}
	cudaEventRecord(PR_eventKernelStop);
	CUDA_CHECK_ERROR(cudaStreamAddCallback(
		stream, PR_i_afterStream, NULL /*kernelTime*/, 0
	), "");
}

// Extension wrapper
PR_HOST void PR_init()
{
	const int sizeMtx = PR_LOCK_TABLE_SIZE * sizeof(int);

	if (PR_lockTableDev == NULL) {
		cudaEventCreate(&PR_eventKernelStart);
		cudaEventCreate(&PR_eventKernelStop);
		PR_lockTableHost = NULL; // TODO: host locks not needed
		// memset(PR_lockTableHost, 0, PR_LOCK_TABLE_SIZE * sizeof(int));
		PR_ALLOC(PR_lockTableDev, sizeMtx);
		PR_CHECK_CUDA_ERROR(cudaMemset(PR_lockTableDev, 0, sizeMtx), "memset");
	}
}
PR_HOST void PR_run(void(*callback)(pr_tx_args_dev_host_s), pr_tx_args_s *pr_args, void *stream)
{
	pr_args->stream = stream;
	PR_i_cudaPrepare(pr_args, callback);
	PR_BEFORE_RUN_EXT(pr_args);
	PR_i_run(pr_args);
	PR_AFTER_RUN_EXT(pr_args);
}
PR_HOST void PR_teardown()
{
	// cudaFreeHost((void*)PR_lockTableHost);
	cudaFree((void*)PR_lockTableDev);
}

PR_DEVICE void PR_beforeKernel_EXT(PR_txCallDefArgs) { PR_BEFORE_KERNEL_EXT(args, pr_args); }
PR_DEVICE void PR_afterKernel_EXT (PR_txCallDefArgs) { PR_AFTER_KERNEL_EXT(args, pr_args);  }
PR_DEVICE void PR_beforeBegin_EXT (PR_txCallDefArgs) { PR_BEFORE_BEGIN_EXT(args, pr_args);  }
PR_DEVICE void PR_afterCommit_EXT (PR_txCallDefArgs) { PR_AFTER_COMMIT_EXT(args, pr_args);  }

#endif /* PR_STM_I_H_GUARD */
